#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
typedef struct data{
	int j,p;
}data;
int main (void){
	data d;
	//borra el FIFO por si ya existía
	unlink("/tmp/FIFO");
	if ((mkfifo("/tmp/FIFO",0600))<0){
		perror("Error al crear la tuberia");
		return -1;
	}
	int fd;
	if((fd = open("/tmp/FIFO",O_RDONLY))<0){
		perror("Error al abrir la tuberia");
		return -1;
	}
	while(read(fd,&d,sizeof(data))==sizeof(data)){
		cout<<"Jugador "<<d.j<<" marca 1 punto, lleva un total de "<<d.p<<" puntos."<<endl;
		if(d.p == 3)
			cout<<"Fin del juego. El jugador "<<d.j<<" ha ganado."<<endl;
	}
}
