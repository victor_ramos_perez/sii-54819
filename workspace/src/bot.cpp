#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
int main(void)
{
	DatosMemCompartida *d;
	void *pvoid; //En C++ no hay conversión implicita de void* a char*. hace falta para el mmap
	int fd;
	if((fd=open("/tmp/shared",O_RDWR))<0){
		perror("No puede abrirse el fichero de memoria compartida");
		return -1;
	}
	struct stat st;
	fstat(fd,&st);
	if((pvoid=(mmap(NULL,st.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0)))==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd);
		return -1;
	}
	close(fd);
	d=(DatosMemCompartida*)pvoid;
	while(1){
		if(d->fin == 1) return 0;
		if(d->esfera.centro.y > d->raqueta1.y1)
			d->accion=1;
		else if (d->esfera.centro.y < d->raqueta1.y2)
			d->accion=-1;
		else d->accion=0;
		usleep(25000);
	}
}
