// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sstream>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	munmap(pdatos,sizeof(datos));
	close(fd);
	unlink("/tmp/FIFO_SERVER");
	close(fd3);
	unlink("/tmp/FIFO_SERVER_KEYS");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
    	char cad[200];
    	read(fd,cad,sizeof(cad));
    	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &esfera.radio, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 
    	
    	//bot
    	pdatos->esfera=esfera;
    	pdatos->raqueta1=jugador1;
    	switch(pdatos->accion)
    	{
    	case 1: OnKeyboardDown('w',1,1);break;
    	case -1:OnKeyboardDown('s',1,1);break;
    	}
    	
    	//fin del juego cuando uno de los jugadores llega a 3 puntos
    	if(puntos1==3 || puntos2==3){
    		pdatos->fin=1;
    		exit(0);
    	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	//Enviar la tecla pulsada al servidor
	write(fd3,&key,sizeof(key));
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	//bot
	if((fd2=open("/tmp/shared",O_CREAT|O_RDWR|O_TRUNC,0666))<0){
		perror("No puede abrirse el fichero de memoria compartida");
		return;
	}
	if((write(fd2,&datos,sizeof(datos)))!=sizeof(datos)){
		perror("Error al escribir en el fichero compartido");
		return;
	}
	struct stat st;
	fstat(fd2,&st);
	if((pvoid=(mmap(NULL,st.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd2,0)))==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd2);
		return;
	}
	close(fd2);
	pdatos=(DatosMemCompartida*)pvoid;
	
	//tuberia con el servidor (recibir datos del servidor)
	unlink("/tmp/FIFO_SERVER");//borra el FIFO por si ya existía
	if ((mkfifo("/tmp/FIFO_SERVER",0600))<0){
		perror("Error al crear la tuberia");
		return;
	}
	if((fd = open("/tmp/FIFO_SERVER",O_RDONLY))<0){
		perror("Error al abrir la tuberia");
		return;
	}
	
	//tuberia con el servidor(enviar las teclas)
	unlink("/tmp/FIFO_SERVER_KEYS");//borra el FIFO por si ya existía
	if ((mkfifo("/tmp/FIFO_SERVER_KEYS",0600))<0){
		perror("Error al crear la tuberia");
		return;
	}
	if((fd3 = open("/tmp/FIFO_SERVER_KEYS",O_WRONLY))<0){
		perror("Error al abrir la tuberia");
		return;
	}
}
