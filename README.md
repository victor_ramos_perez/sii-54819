# JUEGO DEL TENIS

## Instrucciones

Evita que la pelota se salga de la pista por tu lado golpeándola con tu raqueta. 

El ganador será el primer jugador en anotar 3 puntos al rival.

**Cuidado**, a medida que avanza el tiempo, si ningún jugador anota un punto, la bola reducirá su tamaño, haciendo el juego más complicado.

## Controles

### Modo dos jugadores

**Jugador 1**: "w" para moverte hacia arriba y "s" para moverte hacia abajo.

**Jugador 2**: "o" para moverte hacia arriba y "l" para moverte hacia abajo.

### Modo un jugador

**Jugador 1**: controlado por el poderoso tenisbot.

**Jugador 2**: "o" para moverte hacia arriba y "l" para moverte hacia abajo.

