# Changelog

Todos los cambios notables de este proyecto serán documentados en este archivo.

## [4.1]-2021-12-02

### Añadidos
-Uso de un FIFO y threads para enviar la tecla pulsada desde el programa cliente a el programa servidor.
-Modificación del programa bot para que finalice de forma automática cuando el juego termina.
-Nota: el orden en el que deben ser lanzados los programas es: logger-cliente-servidor-bot.

## [4.0]-2021-11-25

### Añadidos
-Creación de los programas cliente y servidor. El servidor envía al cliente los datos del juego (posición y tamaño de esfera y raquetas) empleando un FIFO.

### Retiradas
-Programa tenis. Se ha sustituido por cliente y servidor.

## [3.3]-2021-11-23

### Añadidos
-El juego finaliza cuando uno de los jugadores alcanza 3 puntos, imprimiendo el programa logger el resultado del juego.

## [3.2]-2021-11-23

### Añadidos
-Programa auxiliar Bot que permite controlar la raqueta del jugador 1 de forma autonoma.

## [3.1]-2021-11-18

### Añadidos
-Programa auxiliar Logger que imprime por pantalla los puntos anotados por cada jugador.

## [2.2]-2021-10-27

### Añadidos

-Archivo README.md con instrucciones del juego.

## [2.1]-2021-10-24

### Añadidos

-Reducción del tamaño de la esfera a medida que el tiempo pasa. Si uno de los jugadores anota un punto el tamaño de la esfera vuelve al original.
 
## [2.0]-2021-10-23

### Añadidos

-Movimiento de las raquetas y la esfera.

## [1.1.1]-2021-10-15

### Retiradas

-Carpeta "build" dentro de la carpeta workspace de git.

## [1.1]-2021-10-06

### Añadidos

-Archivo "CHANGELOG" para gestionar la informacion sobre actualizaciones en el futuro.

-Creacion del fichero "build" donde se ha construido el sofware empleando la herramienta CMake. Este fichero contiene los archivos generados por la herramienta, incluyendo la carpeta "src", donde se encuentra un ejecutable.

-Comentario en archivos de cabecera incluyendo nombre para identificar como autor del código.
